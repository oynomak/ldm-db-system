namespace LAND_COMMITEE
{
    partial class LAND_INFORMATION
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LAND_INFORMATION));
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBox_Land_CELLULE = new System.Windows.Forms.ComboBox();
            this.cellsLandBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lAND_COMMITEE_Data_Set = new LAND_COMMITEE.LAND_COMMITEE_Data_Set();
            this.comboBox_LAND_DISTRICT = new System.Windows.Forms.ComboBox();
            this.dISTRICTLandBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox_LAND_SECTEUR = new System.Windows.Forms.ComboBox();
            this.sECTEURLandBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.textBox_DISTRICT = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox_LAND_No = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.textBox_OWNER_NAME = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_OWNER_PRENOM = new System.Windows.Forms.TextBox();
            this.textBox_OWNER_AUTRENOM = new System.Windows.Forms.TextBox();
            this.textBox_No_IDENTITE = new System.Windows.Forms.TextBox();
            this.comboBox_USAGE = new System.Windows.Forms.ComboBox();
            this.textBox_LAND_SIZE = new System.Windows.Forms.TextBox();
            this.comboBox_SUPERVISOR = new System.Windows.Forms.ComboBox();
            this.sUPERVISORBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_FORMER_OWNER = new System.Windows.Forms.ComboBox();
            this.fORMEROWNERSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox_SECTEUR = new System.Windows.Forms.ComboBox();
            this.sECTEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox_CELLULE = new System.Windows.Forms.ComboBox();
            this.cellsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox_DISTRICT = new System.Windows.Forms.ComboBox();
            this.dISTRICTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.comboBox_PROVINCE = new System.Windows.Forms.ComboBox();
            this.pROVINCEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label6 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox_category_New_Ben = new System.Windows.Forms.ComboBox();
            this.beneficiaryCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_category_Former_Owner = new System.Windows.Forms.ComboBox();
            this.formerOwnerCategoryBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.dataGridView_BOUNDARY_DETAILS = new System.Windows.Forms.DataGridView();
            this.lANDNoDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wAYPOINTSDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nORTHINGYDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.eASTINGXDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fROMWAYPOINTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tOWAYPOINTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dISTANCEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bOUNDARYDETAILSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.button_ADD = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lANDINFOBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bOUNDARY_DETAILSTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.BOUNDARY_DETAILSTableAdapter();
            this.sUPERVISORTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SUPERVISORTableAdapter();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.selectMaxNumRefBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lANDOWNERBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lAND_OWNERTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.LAND_OWNERTableAdapter();
            this.pROVINCE_TableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.PROVINCE_TableAdapter();
            this.dISTRICT_TableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.DISTRICT_TableAdapter();
            this.fORMER_OWNERSTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.FORMER_OWNERSTableAdapter();
            this.lAND_INFOTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.LAND_INFOTableAdapter();
            this.select_Max_NumRefTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.Select_Max_NumRefTableAdapter();
            this.sECTEURTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SECTEURTableAdapter();
            this.cellsTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.CellsTableAdapter();
            this.dISTRICT_LandTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.DISTRICT_LandTableAdapter();
            this.sECTEUR_LandTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SECTEUR_LandTableAdapter();
            this.cells_LandTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.Cells_LandTableAdapter();
            this.beneficiaryCategoryTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.BeneficiaryCategoryTableAdapter();
            this.formerOwnerCategoryTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.FormerOwnerCategoryTableAdapter();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cellsLandBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lAND_COMMITEE_Data_Set)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISTRICTLandBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sECTEURLandBindingSource)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sUPERVISORBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORMEROWNERSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sECTEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISTRICTBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROVINCEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.beneficiaryCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.formerOwnerCategoryBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_BOUNDARY_DETAILS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOUNDARYDETAILSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lANDINFOBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectMaxNumRefBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lANDOWNERBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(319, 84);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 17);
            this.label17.TabIndex = 10;
            this.label17.Text = "Surveyor";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(319, 58);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(121, 17);
            this.label16.TabIndex = 17;
            this.label16.Text = "Former Owner";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(319, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 17);
            this.label15.TabIndex = 16;
            this.label15.Text = "Land Size";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(319, 1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(55, 17);
            this.label14.TabIndex = 15;
            this.label14.Text = "Usage";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(49, 80);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 15);
            this.label13.TabIndex = 14;
            this.label13.Text = "Cell";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(33, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 15);
            this.label12.TabIndex = 13;
            this.label12.Text = "Sector";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(29, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(52, 15);
            this.label11.TabIndex = 12;
            this.label11.Text = "District";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox_Land_CELLULE);
            this.groupBox1.Controls.Add(this.comboBox_LAND_DISTRICT);
            this.groupBox1.Controls.Add(this.comboBox_LAND_SECTEUR);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(44, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(361, 108);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "LAND LOCATION";
            // 
            // comboBox_Land_CELLULE
            // 
            this.comboBox_Land_CELLULE.DataSource = this.cellsLandBindingSource;
            this.comboBox_Land_CELLULE.DisplayMember = "CellName";
            this.comboBox_Land_CELLULE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Land_CELLULE.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Land_CELLULE.FormattingEnabled = true;
            this.comboBox_Land_CELLULE.Location = new System.Drawing.Point(139, 77);
            this.comboBox_Land_CELLULE.Name = "comboBox_Land_CELLULE";
            this.comboBox_Land_CELLULE.Size = new System.Drawing.Size(181, 24);
            this.comboBox_Land_CELLULE.TabIndex = 21;
            this.comboBox_Land_CELLULE.ValueMember = "CellID";
            // 
            // cellsLandBindingSource
            // 
            this.cellsLandBindingSource.DataMember = "Cells_Land";
            this.cellsLandBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // lAND_COMMITEE_Data_Set
            // 
            this.lAND_COMMITEE_Data_Set.DataSetName = "LAND_COMMITEE_Data_Set";
            this.lAND_COMMITEE_Data_Set.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // comboBox_LAND_DISTRICT
            // 
            this.comboBox_LAND_DISTRICT.DataSource = this.dISTRICTLandBindingSource;
            this.comboBox_LAND_DISTRICT.DisplayMember = "DESCRIPTION_DISTRICT";
            this.comboBox_LAND_DISTRICT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_LAND_DISTRICT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_LAND_DISTRICT.FormattingEnabled = true;
            this.comboBox_LAND_DISTRICT.Location = new System.Drawing.Point(139, 19);
            this.comboBox_LAND_DISTRICT.Name = "comboBox_LAND_DISTRICT";
            this.comboBox_LAND_DISTRICT.Size = new System.Drawing.Size(181, 24);
            this.comboBox_LAND_DISTRICT.TabIndex = 17;
            this.comboBox_LAND_DISTRICT.ValueMember = "ID_DISTRICT";
            this.comboBox_LAND_DISTRICT.SelectedIndexChanged += new System.EventHandler(this.comboBox_LAND_DISTRICT_SelectedIndexChanged);
            // 
            // dISTRICTLandBindingSource
            // 
            this.dISTRICTLandBindingSource.DataMember = "DISTRICT_Land";
            this.dISTRICTLandBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // comboBox_LAND_SECTEUR
            // 
            this.comboBox_LAND_SECTEUR.DataSource = this.sECTEURLandBindingSource;
            this.comboBox_LAND_SECTEUR.DisplayMember = "DESCRIPTION_SECTEUR";
            this.comboBox_LAND_SECTEUR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_LAND_SECTEUR.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_LAND_SECTEUR.FormattingEnabled = true;
            this.comboBox_LAND_SECTEUR.Location = new System.Drawing.Point(139, 49);
            this.comboBox_LAND_SECTEUR.Name = "comboBox_LAND_SECTEUR";
            this.comboBox_LAND_SECTEUR.Size = new System.Drawing.Size(181, 24);
            this.comboBox_LAND_SECTEUR.TabIndex = 16;
            this.comboBox_LAND_SECTEUR.ValueMember = "ID_SECTEUR";
            this.comboBox_LAND_SECTEUR.SelectedIndexChanged += new System.EventHandler(this.comboBox_LAND_SECTEUR_SelectedIndexChanged);
            // 
            // sECTEURLandBindingSource
            // 
            this.sECTEURLandBindingSource.DataMember = "SECTEUR_Land";
            this.sECTEURLandBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // textBox_DISTRICT
            // 
            this.textBox_DISTRICT.BackColor = System.Drawing.Color.White;
            this.textBox_DISTRICT.Enabled = false;
            this.textBox_DISTRICT.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_DISTRICT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.textBox_DISTRICT.Location = new System.Drawing.Point(416, 60);
            this.textBox_DISTRICT.Name = "textBox_DISTRICT";
            this.textBox_DISTRICT.Size = new System.Drawing.Size(30, 22);
            this.textBox_DISTRICT.TabIndex = 15;
            this.textBox_DISTRICT.Visible = false;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(530, 60);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 22);
            this.button1.TabIndex = 19;
            this.button1.Text = "Generate Land No.";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_LAND_No);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(411, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(361, 49);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            // 
            // textBox_LAND_No
            // 
            this.textBox_LAND_No.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_LAND_No.Enabled = false;
            this.textBox_LAND_No.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_LAND_No.Location = new System.Drawing.Point(110, 15);
            this.textBox_LAND_No.Name = "textBox_LAND_No";
            this.textBox_LAND_No.Size = new System.Drawing.Size(227, 24);
            this.textBox_LAND_No.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(14, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 15);
            this.label1.TabIndex = 16;
            this.label1.Text = "LAND No ";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.87879F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.12121F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 214F));
            this.tableLayoutPanel1.Controls.Add(this.textBox_OWNER_NAME, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox_OWNER_PRENOM, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBox_OWNER_AUTRENOM, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBox_No_IDENTITE, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_USAGE, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_LAND_SIZE, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_SUPERVISOR, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.button3, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label17, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label16, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label15, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label14, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_FORMER_OWNER, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_SECTEUR, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_CELLULE, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.label7, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_DISTRICT, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_PROVINCE, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label21, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_category_New_Ben, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.radioButton1, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.radioButton2, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_category_Former_Owner, 2, 6);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(44, 138);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(721, 256);
            this.tableLayoutPanel1.TabIndex = 22;
            // 
            // textBox_OWNER_NAME
            // 
            this.textBox_OWNER_NAME.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_OWNER_NAME.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_OWNER_NAME.Location = new System.Drawing.Point(123, 4);
            this.textBox_OWNER_NAME.Name = "textBox_OWNER_NAME";
            this.textBox_OWNER_NAME.Size = new System.Drawing.Size(171, 20);
            this.textBox_OWNER_NAME.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(4, 1);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 17);
            this.label2.TabIndex = 18;
            this.label2.Text = "Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(4, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 19;
            this.label3.Text = "Surname";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 20;
            this.label4.Text = "Other Name";
            // 
            // textBox_OWNER_PRENOM
            // 
            this.textBox_OWNER_PRENOM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_OWNER_PRENOM.Location = new System.Drawing.Point(123, 33);
            this.textBox_OWNER_PRENOM.Name = "textBox_OWNER_PRENOM";
            this.textBox_OWNER_PRENOM.Size = new System.Drawing.Size(171, 20);
            this.textBox_OWNER_PRENOM.TabIndex = 23;
            // 
            // textBox_OWNER_AUTRENOM
            // 
            this.textBox_OWNER_AUTRENOM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_OWNER_AUTRENOM.Location = new System.Drawing.Point(123, 61);
            this.textBox_OWNER_AUTRENOM.Name = "textBox_OWNER_AUTRENOM";
            this.textBox_OWNER_AUTRENOM.Size = new System.Drawing.Size(171, 20);
            this.textBox_OWNER_AUTRENOM.TabIndex = 24;
            // 
            // textBox_No_IDENTITE
            // 
            this.textBox_No_IDENTITE.BackColor = System.Drawing.Color.Azure;
            this.textBox_No_IDENTITE.Location = new System.Drawing.Point(123, 87);
            this.textBox_No_IDENTITE.Name = "textBox_No_IDENTITE";
            this.textBox_No_IDENTITE.Size = new System.Drawing.Size(171, 20);
            this.textBox_No_IDENTITE.TabIndex = 28;
            this.textBox_No_IDENTITE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // comboBox_USAGE
            // 
            this.comboBox_USAGE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_USAGE.FormattingEnabled = true;
            this.comboBox_USAGE.Items.AddRange(new object[] {
            "Farming",
            "Cultivation"});
            this.comboBox_USAGE.Location = new System.Drawing.Point(508, 4);
            this.comboBox_USAGE.Name = "comboBox_USAGE";
            this.comboBox_USAGE.Size = new System.Drawing.Size(162, 21);
            this.comboBox_USAGE.TabIndex = 32;
            // 
            // textBox_LAND_SIZE
            // 
            this.textBox_LAND_SIZE.BackColor = System.Drawing.Color.Azure;
            this.textBox_LAND_SIZE.Location = new System.Drawing.Point(508, 33);
            this.textBox_LAND_SIZE.Name = "textBox_LAND_SIZE";
            this.textBox_LAND_SIZE.Size = new System.Drawing.Size(92, 20);
            this.textBox_LAND_SIZE.TabIndex = 33;
            this.textBox_LAND_SIZE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBox_LAND_SIZE.TextChanged += new System.EventHandler(this.textBox_LAND_SIZE_TextChanged);
            // 
            // comboBox_SUPERVISOR
            // 
            this.comboBox_SUPERVISOR.DataSource = this.sUPERVISORBindingSource;
            this.comboBox_SUPERVISOR.DisplayMember = "FullName";
            this.comboBox_SUPERVISOR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_SUPERVISOR.FormattingEnabled = true;
            this.comboBox_SUPERVISOR.Location = new System.Drawing.Point(508, 87);
            this.comboBox_SUPERVISOR.Name = "comboBox_SUPERVISOR";
            this.comboBox_SUPERVISOR.Size = new System.Drawing.Size(192, 21);
            this.comboBox_SUPERVISOR.TabIndex = 35;
            this.comboBox_SUPERVISOR.ValueMember = "No_SUPERVISOR";
            // 
            // sUPERVISORBindingSource
            // 
            this.sUPERVISORBindingSource.DataMember = "SUPERVISOR";
            this.sUPERVISORBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(508, 117);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(192, 21);
            this.button3.TabIndex = 40;
            this.button3.Text = "Former Owner Land Distribution";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_2);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(4, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(112, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "Identity Card";
            // 
            // comboBox_FORMER_OWNER
            // 
            this.comboBox_FORMER_OWNER.DataSource = this.fORMEROWNERSBindingSource;
            this.comboBox_FORMER_OWNER.DisplayMember = "former_owner";
            this.comboBox_FORMER_OWNER.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_FORMER_OWNER.FormattingEnabled = true;
            this.comboBox_FORMER_OWNER.Location = new System.Drawing.Point(508, 61);
            this.comboBox_FORMER_OWNER.Name = "comboBox_FORMER_OWNER";
            this.comboBox_FORMER_OWNER.Size = new System.Drawing.Size(192, 21);
            this.comboBox_FORMER_OWNER.TabIndex = 39;
            this.comboBox_FORMER_OWNER.ValueMember = "ID_FORMER_OWNER";
            // 
            // fORMEROWNERSBindingSource
            // 
            this.fORMEROWNERSBindingSource.DataMember = "FORMER_OWNERS";
            this.fORMEROWNERSBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // comboBox_SECTEUR
            // 
            this.comboBox_SECTEUR.DataSource = this.sECTEURBindingSource;
            this.comboBox_SECTEUR.DisplayMember = "DESCRIPTION_SECTEUR";
            this.comboBox_SECTEUR.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_SECTEUR.FormattingEnabled = true;
            this.comboBox_SECTEUR.Location = new System.Drawing.Point(508, 202);
            this.comboBox_SECTEUR.Name = "comboBox_SECTEUR";
            this.comboBox_SECTEUR.Size = new System.Drawing.Size(171, 21);
            this.comboBox_SECTEUR.TabIndex = 30;
            this.comboBox_SECTEUR.ValueMember = "ID_SECTEUR";
            this.comboBox_SECTEUR.SelectedIndexChanged += new System.EventHandler(this.comboBox_SECTEUR_SelectedIndexChanged);
            // 
            // sECTEURBindingSource
            // 
            this.sECTEURBindingSource.DataMember = "SECTEUR";
            this.sECTEURBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // comboBox_CELLULE
            // 
            this.comboBox_CELLULE.DataSource = this.cellsBindingSource;
            this.comboBox_CELLULE.DisplayMember = "CellName";
            this.comboBox_CELLULE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_CELLULE.FormattingEnabled = true;
            this.comboBox_CELLULE.Location = new System.Drawing.Point(508, 232);
            this.comboBox_CELLULE.Name = "comboBox_CELLULE";
            this.comboBox_CELLULE.Size = new System.Drawing.Size(171, 21);
            this.comboBox_CELLULE.TabIndex = 41;
            this.comboBox_CELLULE.ValueMember = "CellID";
            // 
            // cellsBindingSource
            // 
            this.cellsBindingSource.DataMember = "Cells";
            this.cellsBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(319, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(36, 17);
            this.label7.TabIndex = 26;
            this.label7.Text = "Cell";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(319, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 17);
            this.label8.TabIndex = 27;
            this.label8.Text = "Sector";
            // 
            // comboBox_DISTRICT
            // 
            this.comboBox_DISTRICT.DataSource = this.dISTRICTBindingSource;
            this.comboBox_DISTRICT.DisplayMember = "DESCRIPTION_DISTRICT";
            this.comboBox_DISTRICT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_DISTRICT.FormattingEnabled = true;
            this.comboBox_DISTRICT.Location = new System.Drawing.Point(123, 232);
            this.comboBox_DISTRICT.Name = "comboBox_DISTRICT";
            this.comboBox_DISTRICT.Size = new System.Drawing.Size(171, 21);
            this.comboBox_DISTRICT.TabIndex = 29;
            this.comboBox_DISTRICT.ValueMember = "ID_DISTRICT";
            this.comboBox_DISTRICT.SelectedIndexChanged += new System.EventHandler(this.comboBox_DISTRICT_SelectedIndexChanged);
            // 
            // dISTRICTBindingSource
            // 
            this.dISTRICTBindingSource.DataMember = "DISTRICT";
            this.dISTRICTBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // comboBox_PROVINCE
            // 
            this.comboBox_PROVINCE.DataSource = this.pROVINCEBindingSource;
            this.comboBox_PROVINCE.DisplayMember = "DESCRIPTION_PROVINCE";
            this.comboBox_PROVINCE.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_PROVINCE.FormattingEnabled = true;
            this.comboBox_PROVINCE.Location = new System.Drawing.Point(123, 202);
            this.comboBox_PROVINCE.Name = "comboBox_PROVINCE";
            this.comboBox_PROVINCE.Size = new System.Drawing.Size(171, 21);
            this.comboBox_PROVINCE.TabIndex = 36;
            this.comboBox_PROVINCE.ValueMember = "ID_PROVINCE";
            this.comboBox_PROVINCE.SelectedIndexChanged += new System.EventHandler(this.comboBox_PROVINCE_SelectedIndexChanged_1);
            // 
            // pROVINCEBindingSource
            // 
            this.pROVINCEBindingSource.DataMember = "PROVINCE";
            this.pROVINCEBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 229);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "District";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(4, 199);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(75, 17);
            this.label21.TabIndex = 37;
            this.label21.Text = "Province";
            // 
            // comboBox_category_New_Ben
            // 
            this.comboBox_category_New_Ben.DataSource = this.beneficiaryCategoryBindingSource;
            this.comboBox_category_New_Ben.DisplayMember = "description";
            this.comboBox_category_New_Ben.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_category_New_Ben.FormattingEnabled = true;
            this.comboBox_category_New_Ben.Location = new System.Drawing.Point(123, 175);
            this.comboBox_category_New_Ben.Name = "comboBox_category_New_Ben";
            this.comboBox_category_New_Ben.Size = new System.Drawing.Size(171, 21);
            this.comboBox_category_New_Ben.TabIndex = 43;
            this.comboBox_category_New_Ben.ValueMember = "idcategory";
            // 
            // beneficiaryCategoryBindingSource
            // 
            this.beneficiaryCategoryBindingSource.DataMember = "BeneficiaryCategory";
            this.beneficiaryCategoryBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(123, 147);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(81, 17);
            this.radioButton1.TabIndex = 44;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "New Owner";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(319, 147);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(91, 17);
            this.radioButton2.TabIndex = 45;
            this.radioButton2.Text = "Former Owner";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(4, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 17);
            this.label9.TabIndex = 42;
            this.label9.Text = "Category";
            // 
            // comboBox_category_Former_Owner
            // 
            this.comboBox_category_Former_Owner.DataSource = this.formerOwnerCategoryBindingSource;
            this.comboBox_category_Former_Owner.DisplayMember = "description";
            this.comboBox_category_Former_Owner.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_category_Former_Owner.Enabled = false;
            this.comboBox_category_Former_Owner.FormattingEnabled = true;
            this.comboBox_category_Former_Owner.Location = new System.Drawing.Point(319, 175);
            this.comboBox_category_Former_Owner.Name = "comboBox_category_Former_Owner";
            this.comboBox_category_Former_Owner.Size = new System.Drawing.Size(178, 21);
            this.comboBox_category_Former_Owner.TabIndex = 46;
            this.comboBox_category_Former_Owner.ValueMember = "idcategory";
            // 
            // formerOwnerCategoryBindingSource
            // 
            this.formerOwnerCategoryBindingSource.DataMember = "FormerOwnerCategory";
            this.formerOwnerCategoryBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // dataGridView_BOUNDARY_DETAILS
            // 
            this.dataGridView_BOUNDARY_DETAILS.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dataGridView_BOUNDARY_DETAILS.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView_BOUNDARY_DETAILS.AutoGenerateColumns = false;
            this.dataGridView_BOUNDARY_DETAILS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_BOUNDARY_DETAILS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.lANDNoDataGridViewTextBoxColumn,
            this.wAYPOINTSDataGridViewTextBoxColumn,
            this.nORTHINGYDataGridViewTextBoxColumn,
            this.eASTINGXDataGridViewTextBoxColumn,
            this.fROMWAYPOINTDataGridViewTextBoxColumn,
            this.tOWAYPOINTDataGridViewTextBoxColumn,
            this.dISTANCEDataGridViewTextBoxColumn});
            this.dataGridView_BOUNDARY_DETAILS.DataSource = this.bOUNDARYDETAILSBindingSource;
            this.dataGridView_BOUNDARY_DETAILS.GridColor = System.Drawing.Color.AliceBlue;
            this.dataGridView_BOUNDARY_DETAILS.Location = new System.Drawing.Point(44, 417);
            this.dataGridView_BOUNDARY_DETAILS.MinimumSize = new System.Drawing.Size(609, 150);
            this.dataGridView_BOUNDARY_DETAILS.Name = "dataGridView_BOUNDARY_DETAILS";
            this.dataGridView_BOUNDARY_DETAILS.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.dataGridView_BOUNDARY_DETAILS.Size = new System.Drawing.Size(721, 150);
            this.dataGridView_BOUNDARY_DETAILS.TabIndex = 23;
            this.dataGridView_BOUNDARY_DETAILS.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.dataGridView_BOUNDARY_DETAILS_CellBeginEdit);
            // 
            // lANDNoDataGridViewTextBoxColumn
            // 
            this.lANDNoDataGridViewTextBoxColumn.DataPropertyName = "LAND_No";
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lANDNoDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.lANDNoDataGridViewTextBoxColumn.HeaderText = "LAND_No";
            this.lANDNoDataGridViewTextBoxColumn.Name = "lANDNoDataGridViewTextBoxColumn";
            this.lANDNoDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // wAYPOINTSDataGridViewTextBoxColumn
            // 
            this.wAYPOINTSDataGridViewTextBoxColumn.DataPropertyName = "WAY_POINTS";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            this.wAYPOINTSDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.wAYPOINTSDataGridViewTextBoxColumn.HeaderText = "WAY_POINTS";
            this.wAYPOINTSDataGridViewTextBoxColumn.Name = "wAYPOINTSDataGridViewTextBoxColumn";
            this.wAYPOINTSDataGridViewTextBoxColumn.Width = 90;
            // 
            // nORTHINGYDataGridViewTextBoxColumn
            // 
            this.nORTHINGYDataGridViewTextBoxColumn.DataPropertyName = "NORTHING_Y";
            this.nORTHINGYDataGridViewTextBoxColumn.HeaderText = "NORTHING_Y";
            this.nORTHINGYDataGridViewTextBoxColumn.Name = "nORTHINGYDataGridViewTextBoxColumn";
            this.nORTHINGYDataGridViewTextBoxColumn.Width = 90;
            // 
            // eASTINGXDataGridViewTextBoxColumn
            // 
            this.eASTINGXDataGridViewTextBoxColumn.DataPropertyName = "EASTING_X";
            this.eASTINGXDataGridViewTextBoxColumn.HeaderText = "EASTING_X";
            this.eASTINGXDataGridViewTextBoxColumn.Name = "eASTINGXDataGridViewTextBoxColumn";
            // 
            // fROMWAYPOINTDataGridViewTextBoxColumn
            // 
            this.fROMWAYPOINTDataGridViewTextBoxColumn.DataPropertyName = "FROM_WAYPOINT";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightBlue;
            this.fROMWAYPOINTDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.fROMWAYPOINTDataGridViewTextBoxColumn.HeaderText = "FROM_WAYPOINT";
            this.fROMWAYPOINTDataGridViewTextBoxColumn.Name = "fROMWAYPOINTDataGridViewTextBoxColumn";
            this.fROMWAYPOINTDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tOWAYPOINTDataGridViewTextBoxColumn
            // 
            this.tOWAYPOINTDataGridViewTextBoxColumn.DataPropertyName = "TO_WAYPOINT";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.tOWAYPOINTDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.tOWAYPOINTDataGridViewTextBoxColumn.HeaderText = "TO_WAYPOINT";
            this.tOWAYPOINTDataGridViewTextBoxColumn.Name = "tOWAYPOINTDataGridViewTextBoxColumn";
            // 
            // dISTANCEDataGridViewTextBoxColumn
            // 
            this.dISTANCEDataGridViewTextBoxColumn.DataPropertyName = "DISTANCE";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
            this.dISTANCEDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.dISTANCEDataGridViewTextBoxColumn.HeaderText = "DISTANCE";
            this.dISTANCEDataGridViewTextBoxColumn.Name = "dISTANCEDataGridViewTextBoxColumn";
            this.dISTANCEDataGridViewTextBoxColumn.Width = 70;
            // 
            // bOUNDARYDETAILSBindingSource
            // 
            this.bOUNDARYDETAILSBindingSource.DataMember = "BOUNDARY_DETAILS";
            this.bOUNDARYDETAILSBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Verdana", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label18.Location = new System.Drawing.Point(300, 396);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(181, 18);
            this.label18.TabIndex = 36;
            this.label18.Text = "BOUNDARY DETAILS";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label10.Location = new System.Drawing.Point(41, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(211, 17);
            this.label10.TabIndex = 39;
            this.label10.Text = "OWNER IDENTIFICATION";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Verdana", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label19.Location = new System.Drawing.Point(420, 116);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(124, 17);
            this.label19.TabIndex = 40;
            this.label19.Text = "LAND DETAILS";
            // 
            // button_ADD
            // 
            this.button_ADD.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_ADD.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button_ADD.Image = ((System.Drawing.Image)(resources.GetObject("button_ADD.Image")));
            this.button_ADD.Location = new System.Drawing.Point(543, 573);
            this.button_ADD.Name = "button_ADD";
            this.button_ADD.Size = new System.Drawing.Size(105, 28);
            this.button_ADD.TabIndex = 42;
            this.button_ADD.Text = "  Save";
            this.button_ADD.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button_ADD.UseVisualStyleBackColor = true;
            this.button_ADD.Click += new System.EventHandler(this.button_ADD_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DataSource = this.lANDINFOBindingSource;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(458, 60);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(30, 21);
            this.comboBox1.TabIndex = 45;
            this.comboBox1.Visible = false;
            // 
            // lANDINFOBindingSource
            // 
            this.lANDINFOBindingSource.DataMember = "LAND_INFO";
            this.lANDINFOBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // bOUNDARY_DETAILSTableAdapter
            // 
            this.bOUNDARY_DETAILSTableAdapter.ClearBeforeFill = true;
            // 
            // sUPERVISORTableAdapter
            // 
            this.sUPERVISORTableAdapter.ClearBeforeFill = true;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(494, 61);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(30, 21);
            this.comboBox2.TabIndex = 46;
            this.comboBox2.Visible = false;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(660, 573);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(105, 28);
            this.button2.TabIndex = 47;
            this.button2.Text = "  Close";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // comboBox3
            // 
            this.comboBox3.BackColor = System.Drawing.Color.Silver;
            this.comboBox3.DataSource = this.selectMaxNumRefBindingSource;
            this.comboBox3.DisplayMember = "max_NumReference";
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(1001, 0);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(1, 21);
            this.comboBox3.TabIndex = 48;
            this.comboBox3.ValueMember = "max_NumReference";
            // 
            // selectMaxNumRefBindingSource
            // 
            this.selectMaxNumRefBindingSource.DataMember = "Select_Max_NumRef";
            this.selectMaxNumRefBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // lANDOWNERBindingSource
            // 
            this.lANDOWNERBindingSource.DataMember = "LAND_OWNER";
            this.lANDOWNERBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // lAND_OWNERTableAdapter
            // 
            this.lAND_OWNERTableAdapter.ClearBeforeFill = true;
            // 
            // pROVINCE_TableAdapter
            // 
            this.pROVINCE_TableAdapter.ClearBeforeFill = true;
            // 
            // dISTRICT_TableAdapter
            // 
            this.dISTRICT_TableAdapter.ClearBeforeFill = true;
            // 
            // fORMER_OWNERSTableAdapter
            // 
            this.fORMER_OWNERSTableAdapter.ClearBeforeFill = true;
            // 
            // lAND_INFOTableAdapter
            // 
            this.lAND_INFOTableAdapter.ClearBeforeFill = true;
            // 
            // select_Max_NumRefTableAdapter
            // 
            this.select_Max_NumRefTableAdapter.ClearBeforeFill = true;
            // 
            // sECTEURTableAdapter
            // 
            this.sECTEURTableAdapter.ClearBeforeFill = true;
            // 
            // cellsTableAdapter
            // 
            this.cellsTableAdapter.ClearBeforeFill = true;
            // 
            // dISTRICT_LandTableAdapter
            // 
            this.dISTRICT_LandTableAdapter.ClearBeforeFill = true;
            // 
            // sECTEUR_LandTableAdapter
            // 
            this.sECTEUR_LandTableAdapter.ClearBeforeFill = true;
            // 
            // cells_LandTableAdapter
            // 
            this.cells_LandTableAdapter.ClearBeforeFill = true;
            // 
            // beneficiaryCategoryTableAdapter
            // 
            this.beneficiaryCategoryTableAdapter.ClearBeforeFill = true;
            // 
            // formerOwnerCategoryTableAdapter
            // 
            this.formerOwnerCategoryTableAdapter.ClearBeforeFill = true;
            // 
            // LAND_INFORMATION
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(810, 607);
            this.Controls.Add(this.comboBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.textBox_DISTRICT);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button_ADD);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.dataGridView_BOUNDARY_DETAILS);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(818, 641);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(818, 641);
            this.Name = "LAND_INFORMATION";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LAND COMMITEE : Land Information";
            this.Load += new System.EventHandler(this.LAND_INFORMATION_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cellsLandBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lAND_COMMITEE_Data_Set)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISTRICTLandBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sECTEURLandBindingSource)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sUPERVISORBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fORMEROWNERSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sECTEURBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cellsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISTRICTBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROVINCEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.beneficiaryCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.formerOwnerCategoryBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_BOUNDARY_DETAILS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bOUNDARYDETAILSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lANDINFOBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.selectMaxNumRefBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lANDOWNERBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox_LAND_DISTRICT;
        private System.Windows.Forms.ComboBox comboBox_LAND_SECTEUR;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox textBox_LAND_No;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_OWNER_NAME;
        private System.Windows.Forms.TextBox textBox_OWNER_PRENOM;
        private System.Windows.Forms.TextBox textBox_OWNER_AUTRENOM;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBox_No_IDENTITE;
        private System.Windows.Forms.ComboBox comboBox_DISTRICT;
        private System.Windows.Forms.ComboBox comboBox_SECTEUR;
        private System.Windows.Forms.ComboBox comboBox_USAGE;
        private System.Windows.Forms.TextBox textBox_LAND_SIZE;
        private System.Windows.Forms.ComboBox comboBox_SUPERVISOR;
        private System.Windows.Forms.DataGridView dataGridView_BOUNDARY_DETAILS;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label19;
        private LAND_COMMITEE_Data_Set lAND_COMMITEE_Data_Set;
        private System.Windows.Forms.BindingSource bOUNDARYDETAILSBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.BOUNDARY_DETAILSTableAdapter bOUNDARY_DETAILSTableAdapter;
        private System.Windows.Forms.Button button_ADD;
        private System.Windows.Forms.BindingSource sUPERVISORBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SUPERVISORTableAdapter sUPERVISORTableAdapter;
        private System.Windows.Forms.ComboBox comboBox_PROVINCE;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox_DISTRICT;
        private System.Windows.Forms.DataGridViewTextBoxColumn lANDNoDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn wAYPOINTSDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nORTHINGYDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn eASTINGXDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fROMWAYPOINTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn tOWAYPOINTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dISTANCEDataGridViewTextBoxColumn;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.BindingSource lANDOWNERBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.LAND_OWNERTableAdapter lAND_OWNERTableAdapter;
        private System.Windows.Forms.BindingSource pROVINCEBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.PROVINCE_TableAdapter pROVINCE_TableAdapter;
        private System.Windows.Forms.BindingSource dISTRICTBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.DISTRICT_TableAdapter dISTRICT_TableAdapter;
        private System.Windows.Forms.ComboBox comboBox_FORMER_OWNER;
        private System.Windows.Forms.BindingSource fORMEROWNERSBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.FORMER_OWNERSTableAdapter fORMER_OWNERSTableAdapter;
        private System.Windows.Forms.BindingSource lANDINFOBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.LAND_INFOTableAdapter lAND_INFOTableAdapter;
        private System.Windows.Forms.BindingSource selectMaxNumRefBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.Select_Max_NumRefTableAdapter select_Max_NumRefTableAdapter;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.BindingSource sECTEURBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SECTEURTableAdapter sECTEURTableAdapter;
        private System.Windows.Forms.ComboBox comboBox_CELLULE;
        private System.Windows.Forms.BindingSource cellsBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.CellsTableAdapter cellsTableAdapter;
        private System.Windows.Forms.ComboBox comboBox_Land_CELLULE;
        private System.Windows.Forms.BindingSource dISTRICTLandBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.DISTRICT_LandTableAdapter dISTRICT_LandTableAdapter;
        private System.Windows.Forms.BindingSource cellsLandBindingSource;
        private System.Windows.Forms.BindingSource sECTEURLandBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SECTEUR_LandTableAdapter sECTEUR_LandTableAdapter;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.Cells_LandTableAdapter cells_LandTableAdapter;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox_category_New_Ben;
        private System.Windows.Forms.BindingSource beneficiaryCategoryBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.BeneficiaryCategoryTableAdapter beneficiaryCategoryTableAdapter;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.ComboBox comboBox_category_Former_Owner;
        private System.Windows.Forms.BindingSource formerOwnerCategoryBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.FormerOwnerCategoryTableAdapter formerOwnerCategoryTableAdapter;
    }
}