namespace LAND_COMMITEE
{
    partial class Former_Owners
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Former_Owners));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btLoadImage = new System.Windows.Forms.Button();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.comboBox_Province = new System.Windows.Forms.ComboBox();
            this.comboBox_Cell = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox_Sector = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.comboBox_District = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox_IDCard = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.rdbtGenderFemale = new System.Windows.Forms.RadioButton();
            this.rdbtGenderMale = new System.Windows.Forms.RadioButton();
            this.comboBox_Activity = new System.Windows.Forms.ComboBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox_Former_Size = new System.Windows.Forms.TextBox();
            this.textBox_Other_Name = new System.Windows.Forms.TextBox();
            this.textBox_Last_Name = new System.Windows.Forms.TextBox();
            this.textBox_First_Name = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.comboBox_upd_Province = new System.Windows.Forms.ComboBox();
            this.comboBox_upd_Cell = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.comboBox_upd_Sector = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.comboBox_upd_District = new System.Windows.Forms.ComboBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.btUpdLoadImage = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btNext = new System.Windows.Forms.Button();
            this.comboBox_name = new System.Windows.Forms.ComboBox();
            this.btPrevious = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBox_upd_IDCard = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox_upd_Activity = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.rdbt_upd_GenderFemale = new System.Windows.Forms.RadioButton();
            this.rdbt_upd_GenderMale = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_upd_size = new System.Windows.Forms.TextBox();
            this.textBox_upd_other = new System.Windows.Forms.TextBox();
            this.textBox_upd_surname = new System.Windows.Forms.TextBox();
            this.textBox_upd_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btPrint = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.textBox_main_Act = new System.Windows.Forms.TextBox();
            this.textBox_Gender = new System.Windows.Forms.TextBox();
            this.textBox_distr = new System.Windows.Forms.TextBox();
            this.textBox_prov = new System.Windows.Forms.TextBox();
            this.textBox_sect = new System.Windows.Forms.TextBox();
            this.textBox_cell = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(6, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(587, 345);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(579, 317);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "New";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btLoadImage);
            this.groupBox7.Controls.Add(this.pictureBox);
            this.groupBox7.Location = new System.Drawing.Point(419, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(154, 182);
            this.groupBox7.TabIndex = 17;
            this.groupBox7.TabStop = false;
            // 
            // btLoadImage
            // 
            this.btLoadImage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btLoadImage.Location = new System.Drawing.Point(10, 153);
            this.btLoadImage.Name = "btLoadImage";
            this.btLoadImage.Size = new System.Drawing.Size(134, 23);
            this.btLoadImage.TabIndex = 5;
            this.btLoadImage.Text = "Browse Photo";
            this.btLoadImage.UseVisualStyleBackColor = true;
            this.btLoadImage.Click += new System.EventHandler(this.btLoadImage_Click);
            // 
            // pictureBox
            // 
            this.pictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox.Location = new System.Drawing.Point(10, 12);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(134, 137);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox.TabIndex = 4;
            this.pictureBox.TabStop = false;
            this.pictureBox.DoubleClick += new System.EventHandler(this.pictureBox_DoubleClick);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.comboBox_Province);
            this.groupBox6.Controls.Add(this.comboBox_Cell);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.comboBox_Sector);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.comboBox_District);
            this.groupBox6.Location = new System.Drawing.Point(6, 204);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(567, 67);
            this.groupBox6.TabIndex = 25;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Location";
            // 
            // comboBox_Province
            // 
            this.comboBox_Province.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Province.FormattingEnabled = true;
            this.comboBox_Province.Location = new System.Drawing.Point(97, 11);
            this.comboBox_Province.Name = "comboBox_Province";
            this.comboBox_Province.Size = new System.Drawing.Size(161, 23);
            this.comboBox_Province.TabIndex = 25;
            this.comboBox_Province.Visible = false;
            this.comboBox_Province.SelectedIndexChanged += new System.EventHandler(this.comboBox_Province_SelectedIndexChanged);
            // 
            // comboBox_Cell
            // 
            this.comboBox_Cell.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Cell.FormattingEnabled = true;
            this.comboBox_Cell.Location = new System.Drawing.Point(391, 36);
            this.comboBox_Cell.Name = "comboBox_Cell";
            this.comboBox_Cell.Size = new System.Drawing.Size(161, 23);
            this.comboBox_Cell.TabIndex = 24;
            this.comboBox_Cell.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(38, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 15);
            this.label18.TabIndex = 17;
            this.label18.Text = "Province";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(357, 39);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(30, 15);
            this.label21.TabIndex = 23;
            this.label21.Text = "Cell";
            // 
            // comboBox_Sector
            // 
            this.comboBox_Sector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Sector.FormattingEnabled = true;
            this.comboBox_Sector.Location = new System.Drawing.Point(391, 11);
            this.comboBox_Sector.Name = "comboBox_Sector";
            this.comboBox_Sector.Size = new System.Drawing.Size(161, 23);
            this.comboBox_Sector.TabIndex = 22;
            this.comboBox_Sector.Visible = false;
            this.comboBox_Sector.SelectedIndexChanged += new System.EventHandler(this.comboBox_Sector_SelectedIndexChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(43, 39);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 15);
            this.label19.TabIndex = 19;
            this.label19.Text = "District";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(344, 14);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(43, 15);
            this.label20.TabIndex = 21;
            this.label20.Text = "Sector";
            // 
            // comboBox_District
            // 
            this.comboBox_District.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_District.FormattingEnabled = true;
            this.comboBox_District.Location = new System.Drawing.Point(97, 36);
            this.comboBox_District.Name = "comboBox_District";
            this.comboBox_District.Size = new System.Drawing.Size(161, 23);
            this.comboBox_District.TabIndex = 20;
            this.comboBox_District.Visible = false;
            this.comboBox_District.SelectedIndexChanged += new System.EventHandler(this.comboBox_District_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Location = new System.Drawing.Point(266, 267);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(307, 43);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(184, 13);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "        Save";
            this.button2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(18, 13);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Clear All";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox_IDCard);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.rdbtGenderFemale);
            this.groupBox1.Controls.Add(this.rdbtGenderMale);
            this.groupBox1.Controls.Add(this.comboBox_Activity);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.textBox_Former_Size);
            this.groupBox1.Controls.Add(this.textBox_Other_Name);
            this.groupBox1.Controls.Add(this.textBox_Last_Name);
            this.groupBox1.Controls.Add(this.textBox_First_Name);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(407, 198);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Identification";
            // 
            // textBox_IDCard
            // 
            this.textBox_IDCard.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_IDCard.Location = new System.Drawing.Point(97, 111);
            this.textBox_IDCard.Name = "textBox_IDCard";
            this.textBox_IDCard.Size = new System.Drawing.Size(161, 22);
            this.textBox_IDCard.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(43, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 15);
            this.label9.TabIndex = 15;
            this.label9.Text = "ID Card";
            // 
            // rdbtGenderFemale
            // 
            this.rdbtGenderFemale.AutoSize = true;
            this.rdbtGenderFemale.Location = new System.Drawing.Point(177, 89);
            this.rdbtGenderFemale.Name = "rdbtGenderFemale";
            this.rdbtGenderFemale.Size = new System.Drawing.Size(62, 19);
            this.rdbtGenderFemale.TabIndex = 14;
            this.rdbtGenderFemale.Text = "Female";
            this.rdbtGenderFemale.UseVisualStyleBackColor = true;
            this.rdbtGenderFemale.Click += new System.EventHandler(this.rdbtGenderFemale_Click);
            // 
            // rdbtGenderMale
            // 
            this.rdbtGenderMale.AutoSize = true;
            this.rdbtGenderMale.Checked = true;
            this.rdbtGenderMale.Location = new System.Drawing.Point(99, 89);
            this.rdbtGenderMale.Name = "rdbtGenderMale";
            this.rdbtGenderMale.Size = new System.Drawing.Size(52, 19);
            this.rdbtGenderMale.TabIndex = 13;
            this.rdbtGenderMale.TabStop = true;
            this.rdbtGenderMale.Text = "Male";
            this.rdbtGenderMale.UseVisualStyleBackColor = true;
            this.rdbtGenderMale.Click += new System.EventHandler(this.rdbtGenderMale_Click);
            // 
            // comboBox_Activity
            // 
            this.comboBox_Activity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Activity.FormattingEnabled = true;
            this.comboBox_Activity.Location = new System.Drawing.Point(97, 136);
            this.comboBox_Activity.Name = "comboBox_Activity";
            this.comboBox_Activity.Size = new System.Drawing.Size(219, 23);
            this.comboBox_Activity.TabIndex = 11;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(13, 140);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 15);
            this.label16.TabIndex = 10;
            this.label16.Text = "Main Activity";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(180, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 19);
            this.label10.TabIndex = 8;
            this.label10.Text = "ha";
            // 
            // textBox_Former_Size
            // 
            this.textBox_Former_Size.BackColor = System.Drawing.Color.AliceBlue;
            this.textBox_Former_Size.Location = new System.Drawing.Point(97, 161);
            this.textBox_Former_Size.Name = "textBox_Former_Size";
            this.textBox_Former_Size.Size = new System.Drawing.Size(79, 22);
            this.textBox_Former_Size.TabIndex = 7;
            this.textBox_Former_Size.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_Other_Name
            // 
            this.textBox_Other_Name.Location = new System.Drawing.Point(97, 64);
            this.textBox_Other_Name.Name = "textBox_Other_Name";
            this.textBox_Other_Name.Size = new System.Drawing.Size(219, 22);
            this.textBox_Other_Name.TabIndex = 6;
            // 
            // textBox_Last_Name
            // 
            this.textBox_Last_Name.Location = new System.Drawing.Point(97, 40);
            this.textBox_Last_Name.Name = "textBox_Last_Name";
            this.textBox_Last_Name.Size = new System.Drawing.Size(219, 22);
            this.textBox_Last_Name.TabIndex = 5;
            // 
            // textBox_First_Name
            // 
            this.textBox_First_Name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_First_Name.Location = new System.Drawing.Point(97, 16);
            this.textBox_First_Name.Name = "textBox_First_Name";
            this.textBox_First_Name.Size = new System.Drawing.Size(219, 22);
            this.textBox_First_Name.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(34, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(61, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Land Size";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(19, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Other Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Surname";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(55, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox9);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(579, 317);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Update";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBox_cell);
            this.groupBox9.Controls.Add(this.textBox_sect);
            this.groupBox9.Controls.Add(this.textBox_prov);
            this.groupBox9.Controls.Add(this.textBox_distr);
            this.groupBox9.Controls.Add(this.comboBox_upd_Province);
            this.groupBox9.Controls.Add(this.comboBox_upd_Cell);
            this.groupBox9.Controls.Add(this.label17);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.comboBox_upd_Sector);
            this.groupBox9.Controls.Add(this.label23);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.comboBox_upd_District);
            this.groupBox9.Location = new System.Drawing.Point(5, 203);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(570, 67);
            this.groupBox9.TabIndex = 26;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Location";
            // 
            // comboBox_upd_Province
            // 
            this.comboBox_upd_Province.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_upd_Province.Enabled = false;
            this.comboBox_upd_Province.FormattingEnabled = true;
            this.comboBox_upd_Province.Location = new System.Drawing.Point(91, 14);
            this.comboBox_upd_Province.Name = "comboBox_upd_Province";
            this.comboBox_upd_Province.Size = new System.Drawing.Size(161, 23);
            this.comboBox_upd_Province.TabIndex = 25;
            this.comboBox_upd_Province.Visible = false;
            this.comboBox_upd_Province.SelectedIndexChanged += new System.EventHandler(this.comboBox_upd_Province_SelectedIndexChanged);
            // 
            // comboBox_upd_Cell
            // 
            this.comboBox_upd_Cell.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_upd_Cell.Enabled = false;
            this.comboBox_upd_Cell.FormattingEnabled = true;
            this.comboBox_upd_Cell.Location = new System.Drawing.Point(401, 39);
            this.comboBox_upd_Cell.Name = "comboBox_upd_Cell";
            this.comboBox_upd_Cell.Size = new System.Drawing.Size(161, 23);
            this.comboBox_upd_Cell.TabIndex = 24;
            this.comboBox_upd_Cell.Visible = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(30, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(55, 15);
            this.label17.TabIndex = 17;
            this.label17.Text = "Province";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(357, 42);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(30, 15);
            this.label22.TabIndex = 23;
            this.label22.Text = "Cell";
            // 
            // comboBox_upd_Sector
            // 
            this.comboBox_upd_Sector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_upd_Sector.Enabled = false;
            this.comboBox_upd_Sector.FormattingEnabled = true;
            this.comboBox_upd_Sector.Location = new System.Drawing.Point(401, 14);
            this.comboBox_upd_Sector.Name = "comboBox_upd_Sector";
            this.comboBox_upd_Sector.Size = new System.Drawing.Size(161, 23);
            this.comboBox_upd_Sector.TabIndex = 22;
            this.comboBox_upd_Sector.Visible = false;
            this.comboBox_upd_Sector.SelectedIndexChanged += new System.EventHandler(this.comboBox_upd_Sector_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(35, 42);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(50, 15);
            this.label23.TabIndex = 19;
            this.label23.Text = "District";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(344, 17);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(43, 15);
            this.label24.TabIndex = 21;
            this.label24.Text = "Sector";
            // 
            // comboBox_upd_District
            // 
            this.comboBox_upd_District.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_upd_District.Enabled = false;
            this.comboBox_upd_District.FormattingEnabled = true;
            this.comboBox_upd_District.Location = new System.Drawing.Point(91, 39);
            this.comboBox_upd_District.Name = "comboBox_upd_District";
            this.comboBox_upd_District.Size = new System.Drawing.Size(161, 23);
            this.comboBox_upd_District.TabIndex = 20;
            this.comboBox_upd_District.Visible = false;
            this.comboBox_upd_District.SelectedIndexChanged += new System.EventHandler(this.comboBox_upd_District_SelectedIndexChanged);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.btUpdLoadImage);
            this.groupBox8.Controls.Add(this.pictureBox1);
            this.groupBox8.Location = new System.Drawing.Point(427, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(147, 182);
            this.groupBox8.TabIndex = 18;
            this.groupBox8.TabStop = false;
            // 
            // btUpdLoadImage
            // 
            this.btUpdLoadImage.Enabled = false;
            this.btUpdLoadImage.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btUpdLoadImage.Location = new System.Drawing.Point(6, 153);
            this.btUpdLoadImage.Name = "btUpdLoadImage";
            this.btUpdLoadImage.Size = new System.Drawing.Size(134, 23);
            this.btUpdLoadImage.TabIndex = 5;
            this.btUpdLoadImage.Text = "Browse Photo";
            this.btUpdLoadImage.UseVisualStyleBackColor = true;
            this.btUpdLoadImage.Click += new System.EventHandler(this.btUpdLoadImage_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Enabled = false;
            this.pictureBox1.Location = new System.Drawing.Point(6, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(134, 137);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btNext);
            this.groupBox5.Controls.Add(this.comboBox_name);
            this.groupBox5.Controls.Add(this.btPrevious);
            this.groupBox5.Location = new System.Drawing.Point(5, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(417, 43);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Choose Former Owner";
            // 
            // btNext
            // 
            this.btNext.BackColor = System.Drawing.SystemColors.Control;
            this.btNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btNext.Location = new System.Drawing.Point(341, 16);
            this.btNext.Name = "btNext";
            this.btNext.Size = new System.Drawing.Size(69, 23);
            this.btNext.TabIndex = 71;
            this.btNext.Text = "Next >>";
            this.btNext.UseVisualStyleBackColor = false;
            this.btNext.Click += new System.EventHandler(this.btNext_Click);
            // 
            // comboBox_name
            // 
            this.comboBox_name.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_name.FormattingEnabled = true;
            this.comboBox_name.Location = new System.Drawing.Point(86, 16);
            this.comboBox_name.Name = "comboBox_name";
            this.comboBox_name.Size = new System.Drawing.Size(252, 23);
            this.comboBox_name.TabIndex = 9;
            this.comboBox_name.SelectedIndexChanged += new System.EventHandler(this.comboBox_name_SelectedIndexChanged);
            // 
            // btPrevious
            // 
            this.btPrevious.BackColor = System.Drawing.SystemColors.Control;
            this.btPrevious.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btPrevious.Location = new System.Drawing.Point(4, 16);
            this.btPrevious.Name = "btPrevious";
            this.btPrevious.Size = new System.Drawing.Size(79, 23);
            this.btPrevious.TabIndex = 70;
            this.btPrevious.Text = "<< Previous";
            this.btPrevious.UseVisualStyleBackColor = false;
            this.btPrevious.Click += new System.EventHandler(this.btPrevious_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button5);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Location = new System.Drawing.Point(252, 266);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(322, 45);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            // 
            // button5
            // 
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Location = new System.Drawing.Point(230, 15);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 2;
            this.button5.Text = "Cancel";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(124, 15);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "Save";
            this.button3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.Location = new System.Drawing.Point(18, 15);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 0;
            this.button4.Text = "Update";
            this.button4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBox_Gender);
            this.groupBox4.Controls.Add(this.textBox_main_Act);
            this.groupBox4.Controls.Add(this.textBox_upd_IDCard);
            this.groupBox4.Controls.Add(this.label25);
            this.groupBox4.Controls.Add(this.comboBox_upd_Activity);
            this.groupBox4.Controls.Add(this.label26);
            this.groupBox4.Controls.Add(this.rdbt_upd_GenderFemale);
            this.groupBox4.Controls.Add(this.rdbt_upd_GenderMale);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.textBox_upd_size);
            this.groupBox4.Controls.Add(this.textBox_upd_other);
            this.groupBox4.Controls.Add(this.textBox_upd_surname);
            this.groupBox4.Controls.Add(this.textBox_upd_name);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(5, 47);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(417, 155);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            // 
            // textBox_upd_IDCard
            // 
            this.textBox_upd_IDCard.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_upd_IDCard.Enabled = false;
            this.textBox_upd_IDCard.Location = new System.Drawing.Point(91, 104);
            this.textBox_upd_IDCard.Name = "textBox_upd_IDCard";
            this.textBox_upd_IDCard.Size = new System.Drawing.Size(161, 22);
            this.textBox_upd_IDCard.TabIndex = 20;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(35, 107);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(50, 15);
            this.label25.TabIndex = 19;
            this.label25.Text = "ID Card";
            // 
            // comboBox_upd_Activity
            // 
            this.comboBox_upd_Activity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_upd_Activity.Enabled = false;
            this.comboBox_upd_Activity.FormattingEnabled = true;
            this.comboBox_upd_Activity.Location = new System.Drawing.Point(91, 127);
            this.comboBox_upd_Activity.Name = "comboBox_upd_Activity";
            this.comboBox_upd_Activity.Size = new System.Drawing.Size(219, 23);
            this.comboBox_upd_Activity.TabIndex = 18;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(5, 130);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 15);
            this.label26.TabIndex = 17;
            this.label26.Text = "Main Activity";
            // 
            // rdbt_upd_GenderFemale
            // 
            this.rdbt_upd_GenderFemale.AutoSize = true;
            this.rdbt_upd_GenderFemale.Enabled = false;
            this.rdbt_upd_GenderFemale.Location = new System.Drawing.Point(350, 15);
            this.rdbt_upd_GenderFemale.Name = "rdbt_upd_GenderFemale";
            this.rdbt_upd_GenderFemale.Size = new System.Drawing.Size(62, 19);
            this.rdbt_upd_GenderFemale.TabIndex = 16;
            this.rdbt_upd_GenderFemale.Text = "Female";
            this.rdbt_upd_GenderFemale.UseVisualStyleBackColor = true;
            this.rdbt_upd_GenderFemale.Click += new System.EventHandler(this.rdbt_upd_GenderFemale_Click);
            // 
            // rdbt_upd_GenderMale
            // 
            this.rdbt_upd_GenderMale.AutoSize = true;
            this.rdbt_upd_GenderMale.Checked = true;
            this.rdbt_upd_GenderMale.Enabled = false;
            this.rdbt_upd_GenderMale.Location = new System.Drawing.Point(295, 15);
            this.rdbt_upd_GenderMale.Name = "rdbt_upd_GenderMale";
            this.rdbt_upd_GenderMale.Size = new System.Drawing.Size(52, 19);
            this.rdbt_upd_GenderMale.TabIndex = 15;
            this.rdbt_upd_GenderMale.TabStop = true;
            this.rdbt_upd_GenderMale.Text = "Male";
            this.rdbt_upd_GenderMale.UseVisualStyleBackColor = true;
            this.rdbt_upd_GenderMale.Click += new System.EventHandler(this.rdbt_upd_GenderMale_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bell MT", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(172, 83);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 19);
            this.label11.TabIndex = 9;
            this.label11.Text = "ha";
            // 
            // textBox_upd_size
            // 
            this.textBox_upd_size.BackColor = System.Drawing.Color.AliceBlue;
            this.textBox_upd_size.Enabled = false;
            this.textBox_upd_size.Location = new System.Drawing.Point(91, 81);
            this.textBox_upd_size.Name = "textBox_upd_size";
            this.textBox_upd_size.Size = new System.Drawing.Size(79, 22);
            this.textBox_upd_size.TabIndex = 7;
            this.textBox_upd_size.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // textBox_upd_other
            // 
            this.textBox_upd_other.Enabled = false;
            this.textBox_upd_other.Location = new System.Drawing.Point(91, 58);
            this.textBox_upd_other.Name = "textBox_upd_other";
            this.textBox_upd_other.Size = new System.Drawing.Size(197, 22);
            this.textBox_upd_other.TabIndex = 6;
            // 
            // textBox_upd_surname
            // 
            this.textBox_upd_surname.Enabled = false;
            this.textBox_upd_surname.Location = new System.Drawing.Point(91, 35);
            this.textBox_upd_surname.Name = "textBox_upd_surname";
            this.textBox_upd_surname.Size = new System.Drawing.Size(197, 22);
            this.textBox_upd_surname.TabIndex = 5;
            // 
            // textBox_upd_name
            // 
            this.textBox_upd_name.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textBox_upd_name.Enabled = false;
            this.textBox_upd_name.Location = new System.Drawing.Point(91, 12);
            this.textBox_upd_name.Name = "textBox_upd_name";
            this.textBox_upd_name.Size = new System.Drawing.Size(197, 22);
            this.textBox_upd_name.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 84);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(61, 15);
            this.label5.TabIndex = 3;
            this.label5.Text = "Land Size";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 15);
            this.label6.TabIndex = 2;
            this.label6.Text = "Other Name";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(28, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Surname";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(47, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Name";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.btPrint);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.dataGridView1);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(579, 317);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "List";
            // 
            // btPrint
            // 
            this.btPrint.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btPrint.Image = ((System.Drawing.Image)(resources.GetObject("btPrint.Image")));
            this.btPrint.Location = new System.Drawing.Point(3, 5);
            this.btPrint.Name = "btPrint";
            this.btPrint.Size = new System.Drawing.Size(81, 23);
            this.btPrint.TabIndex = 5;
            this.btPrint.Text = "   Print";
            this.btPrint.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btPrint.UseVisualStyleBackColor = true;
            this.btPrint.Click += new System.EventHandler(this.btPrint_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(476, 292);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 19);
            this.label15.TabIndex = 4;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(401, 292);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(69, 15);
            this.label14.TabIndex = 3;
            this.label14.Text = "Total Size :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(543, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(0, 15);
            this.label13.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(435, 9);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(109, 15);
            this.label12.TabIndex = 1;
            this.label12.Text = "Former Owner(s) :";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            this.dataGridView1.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 32);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(572, 245);
            this.dataGridView1.TabIndex = 0;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // textBox_main_Act
            // 
            this.textBox_main_Act.Location = new System.Drawing.Point(315, 127);
            this.textBox_main_Act.Name = "textBox_main_Act";
            this.textBox_main_Act.Size = new System.Drawing.Size(0, 22);
            this.textBox_main_Act.TabIndex = 21;
            // 
            // textBox_Gender
            // 
            this.textBox_Gender.Location = new System.Drawing.Point(409, 13);
            this.textBox_Gender.Name = "textBox_Gender";
            this.textBox_Gender.Size = new System.Drawing.Size(0, 22);
            this.textBox_Gender.TabIndex = 22;
            // 
            // textBox_distr
            // 
            this.textBox_distr.Location = new System.Drawing.Point(87, 39);
            this.textBox_distr.Name = "textBox_distr";
            this.textBox_distr.Size = new System.Drawing.Size(0, 22);
            this.textBox_distr.TabIndex = 26;
            // 
            // textBox_prov
            // 
            this.textBox_prov.Location = new System.Drawing.Point(87, 14);
            this.textBox_prov.Name = "textBox_prov";
            this.textBox_prov.Size = new System.Drawing.Size(0, 22);
            this.textBox_prov.TabIndex = 27;
            // 
            // textBox_sect
            // 
            this.textBox_sect.Location = new System.Drawing.Point(395, 14);
            this.textBox_sect.Name = "textBox_sect";
            this.textBox_sect.Size = new System.Drawing.Size(0, 22);
            this.textBox_sect.TabIndex = 28;
            // 
            // textBox_cell
            // 
            this.textBox_cell.Location = new System.Drawing.Point(395, 39);
            this.textBox_cell.Name = "textBox_cell";
            this.textBox_cell.Size = new System.Drawing.Size(0, 22);
            this.textBox_cell.TabIndex = 29;
            // 
            // Former_Owners
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(597, 357);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Former_Owners";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LAND COMMITEE : Former Owners";
            this.Load += new System.EventHandler(this.Former_Owners_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox_Former_Size;
        private System.Windows.Forms.TextBox textBox_Other_Name;
        private System.Windows.Forms.TextBox textBox_Last_Name;
        private System.Windows.Forms.TextBox textBox_First_Name;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox comboBox_name;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox textBox_upd_size;
        private System.Windows.Forms.TextBox textBox_upd_other;
        private System.Windows.Forms.TextBox textBox_upd_surname;
        private System.Windows.Forms.TextBox textBox_upd_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox_Activity;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btNext;
        private System.Windows.Forms.Button btPrevious;
        private System.Windows.Forms.Button btPrint;
        private System.Windows.Forms.RadioButton rdbtGenderFemale;
        private System.Windows.Forms.RadioButton rdbtGenderMale;
        private System.Windows.Forms.TextBox textBox_IDCard;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox_Cell;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox_Sector;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBox_District;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBox_Province;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btLoadImage;
        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Button btUpdLoadImage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.ComboBox comboBox_upd_Province;
        private System.Windows.Forms.ComboBox comboBox_upd_Cell;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBox_upd_Sector;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBox_upd_District;
        private System.Windows.Forms.TextBox textBox_upd_IDCard;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ComboBox comboBox_upd_Activity;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.RadioButton rdbt_upd_GenderFemale;
        private System.Windows.Forms.RadioButton rdbt_upd_GenderMale;
        private System.Windows.Forms.TextBox textBox_main_Act;
        private System.Windows.Forms.TextBox textBox_Gender;
        private System.Windows.Forms.TextBox textBox_cell;
        private System.Windows.Forms.TextBox textBox_sect;
        private System.Windows.Forms.TextBox textBox_prov;
        private System.Windows.Forms.TextBox textBox_distr;
    }
}