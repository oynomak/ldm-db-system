namespace LAND_COMMITEE
{
    partial class Ajouter_Prov_Distr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ajouter_Prov_Distr));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button_DELETE_PROV = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button_ADD_PROV = new System.Windows.Forms.Button();
            this.button_UPD_PROV = new System.Windows.Forms.Button();
            this.button_REFLESH_PROV = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView_PROV = new System.Windows.Forms.DataGridView();
            this.iDPROVINCEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRIPTIONPROVINCEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pROVINCEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lAND_COMMITEE_Data_Set = new LAND_COMMITEE.LAND_COMMITEE_Data_Set();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button_DEL_DISTR = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button_ADD_DISTR = new System.Windows.Forms.Button();
            this.button_UPD_DISTR = new System.Windows.Forms.Button();
            this.button_REFRESH_DISTR = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.dataGridView_DISTR = new System.Windows.Forms.DataGridView();
            this.iDDISTRICTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDPROVINCEDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRIPTIONDISTRICTDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dISTRICTBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.button_DEL_SECT = new System.Windows.Forms.Button();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button_ADD_SECT = new System.Windows.Forms.Button();
            this.button_UPD_SECT = new System.Windows.Forms.Button();
            this.button_REFRESH_SECT = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.dataGridView_SECT = new System.Windows.Forms.DataGridView();
            this.iDSECTEURDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDDISTRICTDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dESCRIPTIONSECTEURDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sECTEURBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.sECTEURTableAdapter = new LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SECTEURTableAdapter();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PROV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROVINCEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lAND_COMMITEE_Data_Set)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_DISTR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISTRICTBindingSource)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SECT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sECTEURBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(29, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(541, 310);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(533, 284);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Provinces";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button_DELETE_PROV);
            this.groupBox3.Location = new System.Drawing.Point(416, 148);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(90, 54);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            // 
            // button_DELETE_PROV
            // 
            this.button_DELETE_PROV.Enabled = false;
            this.button_DELETE_PROV.ForeColor = System.Drawing.Color.Red;
            this.button_DELETE_PROV.Location = new System.Drawing.Point(6, 14);
            this.button_DELETE_PROV.Name = "button_DELETE_PROV";
            this.button_DELETE_PROV.Size = new System.Drawing.Size(75, 26);
            this.button_DELETE_PROV.TabIndex = 10;
            this.button_DELETE_PROV.Text = "Delete";
            this.button_DELETE_PROV.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button_ADD_PROV);
            this.groupBox2.Controls.Add(this.button_UPD_PROV);
            this.groupBox2.Controls.Add(this.button_REFLESH_PROV);
            this.groupBox2.Location = new System.Drawing.Point(416, 20);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(90, 122);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            // 
            // button_ADD_PROV
            // 
            this.button_ADD_PROV.Enabled = false;
            this.button_ADD_PROV.Location = new System.Drawing.Point(6, 19);
            this.button_ADD_PROV.Name = "button_ADD_PROV";
            this.button_ADD_PROV.Size = new System.Drawing.Size(75, 26);
            this.button_ADD_PROV.TabIndex = 9;
            this.button_ADD_PROV.Text = "Add";
            this.button_ADD_PROV.UseVisualStyleBackColor = true;
            this.button_ADD_PROV.Click += new System.EventHandler(this.button_ADD_PROV_Click);
            // 
            // button_UPD_PROV
            // 
            this.button_UPD_PROV.Enabled = false;
            this.button_UPD_PROV.Location = new System.Drawing.Point(6, 51);
            this.button_UPD_PROV.Name = "button_UPD_PROV";
            this.button_UPD_PROV.Size = new System.Drawing.Size(75, 26);
            this.button_UPD_PROV.TabIndex = 12;
            this.button_UPD_PROV.Text = "Update";
            this.button_UPD_PROV.UseVisualStyleBackColor = true;
            // 
            // button_REFLESH_PROV
            // 
            this.button_REFLESH_PROV.Enabled = false;
            this.button_REFLESH_PROV.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button_REFLESH_PROV.Location = new System.Drawing.Point(6, 83);
            this.button_REFLESH_PROV.Name = "button_REFLESH_PROV";
            this.button_REFLESH_PROV.Size = new System.Drawing.Size(75, 26);
            this.button_REFLESH_PROV.TabIndex = 13;
            this.button_REFLESH_PROV.Text = "Refresh";
            this.button_REFLESH_PROV.UseVisualStyleBackColor = true;
            this.button_REFLESH_PROV.Click += new System.EventHandler(this.button_REFLESH_PROV_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView_PROV);
            this.groupBox1.Location = new System.Drawing.Point(26, 20);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(384, 182);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Province";
            // 
            // dataGridView_PROV
            // 
            this.dataGridView_PROV.AllowUserToOrderColumns = true;
            this.dataGridView_PROV.AutoGenerateColumns = false;
            this.dataGridView_PROV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_PROV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDPROVINCEDataGridViewTextBoxColumn,
            this.dESCRIPTIONPROVINCEDataGridViewTextBoxColumn});
            this.dataGridView_PROV.DataSource = this.pROVINCEBindingSource;
            this.dataGridView_PROV.Location = new System.Drawing.Point(12, 19);
            this.dataGridView_PROV.Name = "dataGridView_PROV";
            this.dataGridView_PROV.Size = new System.Drawing.Size(361, 150);
            this.dataGridView_PROV.TabIndex = 11;
            // 
            // iDPROVINCEDataGridViewTextBoxColumn
            // 
            this.iDPROVINCEDataGridViewTextBoxColumn.DataPropertyName = "ID_PROVINCE";
            this.iDPROVINCEDataGridViewTextBoxColumn.HeaderText = "ID_PROVINCE";
            this.iDPROVINCEDataGridViewTextBoxColumn.Name = "iDPROVINCEDataGridViewTextBoxColumn";
            // 
            // dESCRIPTIONPROVINCEDataGridViewTextBoxColumn
            // 
            this.dESCRIPTIONPROVINCEDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPTION_PROVINCE";
            this.dESCRIPTIONPROVINCEDataGridViewTextBoxColumn.HeaderText = "DESCRIPTION_PROVINCE";
            this.dESCRIPTIONPROVINCEDataGridViewTextBoxColumn.Name = "dESCRIPTIONPROVINCEDataGridViewTextBoxColumn";
            // 
            // pROVINCEBindingSource
            // 
            this.pROVINCEBindingSource.DataMember = "PROVINCE";
            this.pROVINCEBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // lAND_COMMITEE_Data_Set
            // 
            this.lAND_COMMITEE_Data_Set.DataSetName = "LAND_COMMITEE_Data_Set";
            this.lAND_COMMITEE_Data_Set.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox8);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(533, 284);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Districts";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.button_DEL_DISTR);
            this.groupBox6.Location = new System.Drawing.Point(416, 175);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(90, 54);
            this.groupBox6.TabIndex = 20;
            this.groupBox6.TabStop = false;
            // 
            // button_DEL_DISTR
            // 
            this.button_DEL_DISTR.Enabled = false;
            this.button_DEL_DISTR.ForeColor = System.Drawing.Color.Red;
            this.button_DEL_DISTR.Location = new System.Drawing.Point(6, 14);
            this.button_DEL_DISTR.Name = "button_DEL_DISTR";
            this.button_DEL_DISTR.Size = new System.Drawing.Size(75, 26);
            this.button_DEL_DISTR.TabIndex = 10;
            this.button_DEL_DISTR.Text = "Delete";
            this.button_DEL_DISTR.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button_ADD_DISTR);
            this.groupBox7.Controls.Add(this.button_UPD_DISTR);
            this.groupBox7.Controls.Add(this.button_REFRESH_DISTR);
            this.groupBox7.Location = new System.Drawing.Point(416, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(90, 149);
            this.groupBox7.TabIndex = 19;
            this.groupBox7.TabStop = false;
            // 
            // button_ADD_DISTR
            // 
            this.button_ADD_DISTR.Enabled = false;
            this.button_ADD_DISTR.Location = new System.Drawing.Point(6, 19);
            this.button_ADD_DISTR.Name = "button_ADD_DISTR";
            this.button_ADD_DISTR.Size = new System.Drawing.Size(75, 26);
            this.button_ADD_DISTR.TabIndex = 9;
            this.button_ADD_DISTR.Text = "Add";
            this.button_ADD_DISTR.UseVisualStyleBackColor = true;
            this.button_ADD_DISTR.Click += new System.EventHandler(this.button_ADD_DISTR_Click);
            // 
            // button_UPD_DISTR
            // 
            this.button_UPD_DISTR.Enabled = false;
            this.button_UPD_DISTR.Location = new System.Drawing.Point(6, 62);
            this.button_UPD_DISTR.Name = "button_UPD_DISTR";
            this.button_UPD_DISTR.Size = new System.Drawing.Size(75, 26);
            this.button_UPD_DISTR.TabIndex = 12;
            this.button_UPD_DISTR.Text = "Update";
            this.button_UPD_DISTR.UseVisualStyleBackColor = true;
            // 
            // button_REFRESH_DISTR
            // 
            this.button_REFRESH_DISTR.Enabled = false;
            this.button_REFRESH_DISTR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button_REFRESH_DISTR.Location = new System.Drawing.Point(6, 104);
            this.button_REFRESH_DISTR.Name = "button_REFRESH_DISTR";
            this.button_REFRESH_DISTR.Size = new System.Drawing.Size(75, 26);
            this.button_REFRESH_DISTR.TabIndex = 13;
            this.button_REFRESH_DISTR.Text = "Refresh";
            this.button_REFRESH_DISTR.UseVisualStyleBackColor = true;
            this.button_REFRESH_DISTR.Click += new System.EventHandler(this.button_REFRESH_DISTR_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this.comboBox4);
            this.groupBox8.Controls.Add(this.dataGridView_DISTR);
            this.groupBox8.Location = new System.Drawing.Point(26, 20);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(384, 209);
            this.groupBox8.TabIndex = 18;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Districts";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Province";
            // 
            // comboBox4
            // 
            this.comboBox4.DataSource = this.pROVINCEBindingSource;
            this.comboBox4.DisplayMember = "DESCRIPTION_PROVINCE";
            this.comboBox4.Enabled = false;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(64, 19);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(157, 21);
            this.comboBox4.TabIndex = 12;
            this.comboBox4.ValueMember = "ID_PROVINCE";
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.comboBox4_SelectedIndexChanged);
            // 
            // dataGridView_DISTR
            // 
            this.dataGridView_DISTR.AllowUserToOrderColumns = true;
            this.dataGridView_DISTR.AutoGenerateColumns = false;
            this.dataGridView_DISTR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_DISTR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDDISTRICTDataGridViewTextBoxColumn,
            this.iDPROVINCEDataGridViewTextBoxColumn1,
            this.dESCRIPTIONDISTRICTDataGridViewTextBoxColumn});
            this.dataGridView_DISTR.DataSource = this.dISTRICTBindingSource;
            this.dataGridView_DISTR.Location = new System.Drawing.Point(12, 45);
            this.dataGridView_DISTR.Name = "dataGridView_DISTR";
            this.dataGridView_DISTR.Size = new System.Drawing.Size(361, 150);
            this.dataGridView_DISTR.TabIndex = 11;
            // 
            // iDDISTRICTDataGridViewTextBoxColumn
            // 
            this.iDDISTRICTDataGridViewTextBoxColumn.DataPropertyName = "ID_DISTRICT";
            this.iDDISTRICTDataGridViewTextBoxColumn.HeaderText = "ID_DISTRICT";
            this.iDDISTRICTDataGridViewTextBoxColumn.Name = "iDDISTRICTDataGridViewTextBoxColumn";
            // 
            // iDPROVINCEDataGridViewTextBoxColumn1
            // 
            this.iDPROVINCEDataGridViewTextBoxColumn1.DataPropertyName = "ID_PROVINCE";
            this.iDPROVINCEDataGridViewTextBoxColumn1.HeaderText = "ID_PROVINCE";
            this.iDPROVINCEDataGridViewTextBoxColumn1.Name = "iDPROVINCEDataGridViewTextBoxColumn1";
            // 
            // dESCRIPTIONDISTRICTDataGridViewTextBoxColumn
            // 
            this.dESCRIPTIONDISTRICTDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPTION_DISTRICT";
            this.dESCRIPTIONDISTRICTDataGridViewTextBoxColumn.HeaderText = "DESCRIPTION_DISTRICT";
            this.dESCRIPTIONDISTRICTDataGridViewTextBoxColumn.Name = "dESCRIPTIONDISTRICTDataGridViewTextBoxColumn";
            // 
            // dISTRICTBindingSource
            // 
            this.dISTRICTBindingSource.DataMember = "DISTRICT";
            this.dISTRICTBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox10);
            this.tabPage3.Controls.Add(this.groupBox11);
            this.tabPage3.Controls.Add(this.groupBox12);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(533, 284);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Sectors";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.button_DEL_SECT);
            this.groupBox10.Location = new System.Drawing.Point(416, 173);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(90, 54);
            this.groupBox10.TabIndex = 20;
            this.groupBox10.TabStop = false;
            // 
            // button_DEL_SECT
            // 
            this.button_DEL_SECT.Enabled = false;
            this.button_DEL_SECT.ForeColor = System.Drawing.Color.Red;
            this.button_DEL_SECT.Location = new System.Drawing.Point(6, 14);
            this.button_DEL_SECT.Name = "button_DEL_SECT";
            this.button_DEL_SECT.Size = new System.Drawing.Size(75, 26);
            this.button_DEL_SECT.TabIndex = 10;
            this.button_DEL_SECT.Text = "Delete";
            this.button_DEL_SECT.UseVisualStyleBackColor = true;
            this.button_DEL_SECT.Click += new System.EventHandler(this.button_DEL_SECT_Click);
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button_ADD_SECT);
            this.groupBox11.Controls.Add(this.button_UPD_SECT);
            this.groupBox11.Controls.Add(this.button_REFRESH_SECT);
            this.groupBox11.Location = new System.Drawing.Point(416, 20);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(90, 147);
            this.groupBox11.TabIndex = 19;
            this.groupBox11.TabStop = false;
            // 
            // button_ADD_SECT
            // 
            this.button_ADD_SECT.Enabled = false;
            this.button_ADD_SECT.Location = new System.Drawing.Point(6, 19);
            this.button_ADD_SECT.Name = "button_ADD_SECT";
            this.button_ADD_SECT.Size = new System.Drawing.Size(75, 26);
            this.button_ADD_SECT.TabIndex = 9;
            this.button_ADD_SECT.Text = "Add";
            this.button_ADD_SECT.UseVisualStyleBackColor = true;
            this.button_ADD_SECT.Click += new System.EventHandler(this.button_ADD_SECT_Click);
            // 
            // button_UPD_SECT
            // 
            this.button_UPD_SECT.Enabled = false;
            this.button_UPD_SECT.Location = new System.Drawing.Point(6, 64);
            this.button_UPD_SECT.Name = "button_UPD_SECT";
            this.button_UPD_SECT.Size = new System.Drawing.Size(75, 26);
            this.button_UPD_SECT.TabIndex = 12;
            this.button_UPD_SECT.Text = "Update";
            this.button_UPD_SECT.UseVisualStyleBackColor = true;
            // 
            // button_REFRESH_SECT
            // 
            this.button_REFRESH_SECT.Enabled = false;
            this.button_REFRESH_SECT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button_REFRESH_SECT.Location = new System.Drawing.Point(6, 106);
            this.button_REFRESH_SECT.Name = "button_REFRESH_SECT";
            this.button_REFRESH_SECT.Size = new System.Drawing.Size(75, 26);
            this.button_REFRESH_SECT.TabIndex = 13;
            this.button_REFRESH_SECT.Text = "Refresh";
            this.button_REFRESH_SECT.UseVisualStyleBackColor = true;
            this.button_REFRESH_SECT.Click += new System.EventHandler(this.button_REFRESH_SECT_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.label6);
            this.groupBox12.Controls.Add(this.comboBox5);
            this.groupBox12.Controls.Add(this.dataGridView_SECT);
            this.groupBox12.Location = new System.Drawing.Point(26, 20);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(384, 207);
            this.groupBox12.TabIndex = 18;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Sectors";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "District";
            // 
            // comboBox5
            // 
            this.comboBox5.DataSource = this.dISTRICTBindingSource;
            this.comboBox5.DisplayMember = "DESCRIPTION_DISTRICT";
            this.comboBox5.Enabled = false;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(66, 19);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(157, 21);
            this.comboBox5.TabIndex = 14;
            this.comboBox5.ValueMember = "ID_DISTRICT";
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // dataGridView_SECT
            // 
            this.dataGridView_SECT.AllowUserToOrderColumns = true;
            this.dataGridView_SECT.AutoGenerateColumns = false;
            this.dataGridView_SECT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_SECT.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDSECTEURDataGridViewTextBoxColumn,
            this.iDDISTRICTDataGridViewTextBoxColumn1,
            this.dESCRIPTIONSECTEURDataGridViewTextBoxColumn});
            this.dataGridView_SECT.DataSource = this.sECTEURBindingSource;
            this.dataGridView_SECT.Location = new System.Drawing.Point(12, 45);
            this.dataGridView_SECT.Name = "dataGridView_SECT";
            this.dataGridView_SECT.Size = new System.Drawing.Size(361, 150);
            this.dataGridView_SECT.TabIndex = 11;
            // 
            // iDSECTEURDataGridViewTextBoxColumn
            // 
            this.iDSECTEURDataGridViewTextBoxColumn.DataPropertyName = "ID_SECTEUR";
            this.iDSECTEURDataGridViewTextBoxColumn.HeaderText = "ID_SECTEUR";
            this.iDSECTEURDataGridViewTextBoxColumn.Name = "iDSECTEURDataGridViewTextBoxColumn";
            // 
            // iDDISTRICTDataGridViewTextBoxColumn1
            // 
            this.iDDISTRICTDataGridViewTextBoxColumn1.DataPropertyName = "ID_DISTRICT";
            this.iDDISTRICTDataGridViewTextBoxColumn1.HeaderText = "ID_DISTRICT";
            this.iDDISTRICTDataGridViewTextBoxColumn1.Name = "iDDISTRICTDataGridViewTextBoxColumn1";
            // 
            // dESCRIPTIONSECTEURDataGridViewTextBoxColumn
            // 
            this.dESCRIPTIONSECTEURDataGridViewTextBoxColumn.DataPropertyName = "DESCRIPTION_SECTEUR";
            this.dESCRIPTIONSECTEURDataGridViewTextBoxColumn.HeaderText = "DESCRIPTION_SECTEUR";
            this.dESCRIPTIONSECTEURDataGridViewTextBoxColumn.Name = "dESCRIPTIONSECTEURDataGridViewTextBoxColumn";
            // 
            // sECTEURBindingSource
            // 
            this.sECTEURBindingSource.DataMember = "SECTEUR";
            this.sECTEURBindingSource.DataSource = this.lAND_COMMITEE_Data_Set;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(297, 31);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(76, 13);
            this.linkLabel2.TabIndex = 2;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Next to District";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Choose Province";
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(126, 28);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(157, 21);
            this.comboBox2.TabIndex = 0;
            this.comboBox2.Text = "---Select Province---";
            // 
            // button5
            // 
            this.button5.ForeColor = System.Drawing.Color.Red;
            this.button5.Location = new System.Drawing.Point(6, 14);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 26);
            this.button5.TabIndex = 10;
            this.button5.Text = "Delete";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(6, 19);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 26);
            this.button6.TabIndex = 9;
            this.button6.Text = "Add";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(6, 51);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 26);
            this.button7.TabIndex = 12;
            this.button7.Text = "Update";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.ForeColor = System.Drawing.Color.DodgerBlue;
            this.button8.Location = new System.Drawing.Point(6, 83);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 26);
            this.button8.TabIndex = 13;
            this.button8.Text = "Refresh";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(12, 19);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(361, 150);
            this.dataGridView2.TabIndex = 11;
            // 
            // sECTEURTableAdapter
            // 
            this.sECTEURTableAdapter.ClearBeforeFill = true;
            // 
            // Ajouter_Prov_Distr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(607, 379);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(615, 413);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(615, 413);
            this.Name = "Ajouter_Prov_Distr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LAND COMMITEE : Ajout des Provinces, Districts et Secteurs";
            this.Load += new System.EventHandler(this.Ajouter_Prov_Distr_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_PROV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pROVINCEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lAND_COMMITEE_Data_Set)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_DISTR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dISTRICTBindingSource)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SECT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sECTEURBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button button_REFLESH_PROV;
        private System.Windows.Forms.Button button_UPD_PROV;
        private System.Windows.Forms.DataGridView dataGridView_PROV;
        private System.Windows.Forms.Button button_DELETE_PROV;
        private System.Windows.Forms.Button button_ADD_PROV;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button_DEL_DISTR;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button_ADD_DISTR;
        private System.Windows.Forms.Button button_UPD_DISTR;
        private System.Windows.Forms.Button button_REFRESH_DISTR;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dataGridView_DISTR;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button button_DEL_SECT;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button_ADD_SECT;
        private System.Windows.Forms.Button button_UPD_SECT;
        private System.Windows.Forms.Button button_REFRESH_SECT;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.DataGridView dataGridView_SECT;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox4;
        private LAND_COMMITEE_Data_Set lAND_COMMITEE_Data_Set;
        private System.Windows.Forms.BindingSource pROVINCEBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDPROVINCEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRIPTIONPROVINCEDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource dISTRICTBindingSource;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDISTRICTDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDPROVINCEDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRIPTIONDISTRICTDataGridViewTextBoxColumn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.BindingSource sECTEURBindingSource;
        private LAND_COMMITEE.LAND_COMMITEE_Data_SetTableAdapters.SECTEURTableAdapter sECTEURTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDSECTEURDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDISTRICTDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dESCRIPTIONSECTEURDataGridViewTextBoxColumn;
    }
}